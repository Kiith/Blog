Configuring Neovim for C++ from scratch
=======================================


This is a step by step guide through setup of a minimal working C++ Neovim
configuration using all the new-ish LSP/tree-sitter features, including various
problems I encountered along the way. 

This is the best 'smart' C++ setup I've had in Neovim so far.
Autocompletion, go-to-definition, references to symbol, automated refactoring,
code actions, etc. 'just work', even where IDEs like Eclipse prefer suicide
when faced with some... imposing corporate codebases.


If you don't care about the explanation and just want to have a working C++
config, skip to TLDR_ below. 

Base
----

I assume you have the latest Neovim release (0.6 as of writing).  You can get
it `here <https://github.com/neovim/neovim/releases/latest>`_.  Most Linux
distro repositories ship older versions, which will not work (many plugins require 0.6).

I started with the mjlbach's `kickstart.nvim
<https://github.com/nvim-lua/kickstart.nvim>`_ config as a base.  You will need
to replace (backup first!) the contents of ``~/.config/nvim/`` with this
repository.  This already does a large part of the work for us.  Also make sure
to clear/backup the contents of your ``~/.local/share/nvim/`` (where Neovim
stores plugins, among other things).

Also, you will need to install ripgrep (``rg``) for the config to work. Ubuntu
packages were broken for some reason, so  I used the version from ``cargo``::

   sudo apt install cargo #change as needed for your OS
   cargo install ripgrep



Cleaning up the plugins
-----------------------

I did not need all plugins used by ``kickstart.nvim``:

- Removed ``use 'ludovicchabant/vim-gutentags'``

  Tags never worked well for C++, and LSP completely obsoletes them

- I also removed ``tpope/vim-fugitive`` and ``tpope/vim-rhubarb`` as I have no
  use for them; if you also do this, make sure to also remove ``gitbranch`` and
  the ``component_function`` from the ``lightline`` configuration. There are
  more lightweight ways to do that, maybe I'll write that up later.
- Note that ``nvim-cmp`` `apparently
  <https://github.com/hrsh7th/nvim-cmp#recommended-configuration>`_ currently
  requires *some* snippet plugin to work, so don't remove ``luasnips`` even if
  you don't use it.


Working around issues
---------------------


I changed some of the settings to get around issues I ran into:

- In ``require('telescope').setup`` I commented out ``<C-u>``/``<C-d>`` disables::

     -- ['<C-u>'] = false,
     -- ['<C-d>'] = false,

  These are used for scrolling the Telescope preview window (see below), and I found that to
  be too useful to be disabled.
- In ``require("nvim-treesitter.configs").setup()`` I disabled the ``indent`` feature::
    
    indent = {
      enable = false,
    }

  This feature caused some really annoying auto-indents on a new line. This is *probably* something
  related to a particular indentation config and treesitter's C++ implementation.
- I removed the LSP capabilities update for ``nvim-cmp``::

    -- capabilities = require('cmp_nvim_lsp').update_capabilities(capabilities)

  This makes autocompletion slightly dumber (it doesn't generate function
  parameter placeholders), but avoids an annoying bug where ``cmp-nvim`` puts
  the cursor in a wrong location on tab-indented lines.
- ``local servers = { 'ccls' }``

  I had a bad experience with ``clangd`` way back, so I use ``ccls``
  instead, and I didn't need any other language servers.
- I removed the 'example custom server' code, since that's just  an example
  (remove all ``sumneko`` related code)


Base config changes
-------------------

- ``use {'nvim-treesitter/nvim-treesitter', run = ':TSUpdate'}``

  Rewriting the ``use 'nvim-treesitter/nvim-tresitter'`` to this ensures
  treesitter languages are updated when you update plugins (``:PackerUpdate``).

- ``ensure_installed = "maintained"``

  Added this to ``require('nvim-treesitter.configs').setup {`` . This installs
  Treesitter support for all important languages including C++ without needing
  to manually install it later.  Treesitter gives us better highlighting and
  some newer Neovim features like incremental selection, and is used by many
  plugins.

  Treesitter even works with multiple languages in a single file; nested code
  blocks, Doxygen comments, etc. will all be highlighted.

First run
---------

* Launch ``nvim``. Ignore any errors due to missing plugins (our config won't work until we install the plugins)
* Run ``:PackerInstall`` - this will install the plugins.
  - Note: I got an error, failing to install ``nvim-treesitter/nvim-treesitter-textobjects``.
    But then I found it installed in ``~/.local/share/nvim`` and had no further errors, so I'm not sure what happened there.
* restart ``nvim``

Setting up ``ccls``
-------------------

``ccls`` is an LSP server - it indexes your codebase and talks to the LSP
client in Neovim.  This enables autocompletion, go-to-definition, highlighting
of errors/warnings and other language-aware features.

It's best to install ``ccls`` from your system's repositories to keep it in
sync with your ``clang`` version.  On Debian/Ubuntu derivatives, you can do
this with::

   sudo apt install ccls

Next, we need to configure ``ccls`` from ``init.lua``. Add this somewhere below ``local lspconfig``::

   lspconfig.ccls.setup {
     -- I couldn't get this override (removing .git) to work, see paragraph below
     -- root_dir = nvim_lsp.util.root_pattern( "compile_commands.json", ".ccls" ),
     init_options = {
       -- these options are passed directly to the LSP server (ccls in our case).
       -- ccls options documented here: https://github.com/MaskRay/ccls/wiki/Customization
       index = {
         -- reasonable value on an 8-core (using all cores can slow the machine down during indexing).
         threads = 6
       }
     }
   }

Neovim LSP determines the 'root directory' of a project by going up from the
working directory until it finds a file matching the ``root_dir`` pattern.  For
most simple projects, this is something like ``.git``.  ``ccls`` will then
index all files in this root directory.

**Note**: I needed to override ``root_dir`` for a project with git submodules
where ``.git`` cannot work as a root marker. I couldn't get this to work by
setting ``root_dir`` as commented out above (bug?), so I ended up removing
``".git"`` from ``root_dir`` in ``nvim-lspconfig`` installation directory in
``~/.local/share/nvim/site/pack/packer/start/nvim-lspconfig/lua/lspconfig/server_configurations/ccls.lua``.
This path may vary if you if you don't use ``packer`` or depending on OS.

To determine how to build the indexed files, ``ccls`` needs files 
`.ccls and/or compile_commands.json <https://github.com/MaskRay/ccls/wiki/Project-Setup#ccls-file>`_
in the root directory. See next sections for how to use Bear to generate
``compile_commands.json``.

For more details on Neovim LSP config, see: `nvim-lspconfig <https://github.com/neovim/nvim-lspconfig>`_


Getting build information for your project with Bear
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

`Bear <https://github.com/rizsotto/Bear>`_ analyzes your build process and
generates ``compile_commands.json`` for use with ``ccls``. Some build systems
(e.g. ``cmake``) can generate ``compile_commands.json`` directly, but Bear
works almost anywhere, including obscure build systems in corporate
environments.

Install ``bear`` first. On Debian/Ubuntu derivatives, you can run::

   sudo apt install bear

Other systems also have Bear in their repositories. For those that don't, see
the `manual installation instructions <https://github.com/rizsotto/Bear/blob/master/INSTALL.md>`_

Now go to the root directory of your project, (the directory ``ccls`` should
treat as the root), and run you build through ``bear``. E.g. for a SCons-base
build, we'd do::

   bear scons --my-scons-parameters

or in newest version of ``bear``, if you build from source::

   bear -- scons --my-scons-parameters

As of December 2021, I recommend against building from sources as I had some
crashes I couldn't figure out.

This should generate a ``compile_commands.json`` in the current (root) directory.

Run with LSP
------------

* Launch ``nvim`` again.
* Open a C++ file somewhere in/under your root directory.
* Check if LSP is enabled with ``:LspInfo``. It should show at least 1 client attached to this buffer.
  - If there are 0 clients, run ``:LspStart`` and try again - it seems this is needed the first time you open a C++ file - not sure why.

Completion suggestions should show up immediately as you type. 

* Use ``<Tab>``/``<S-Tab>`` to select/apply completions, or ``<Up>``/``<Down>``+``<Enter>`` to separately select and apply.
* Try ``gd``/``gr`` on a function/variable/type in normal mode to go to its definition/references.
* Try writing some incorrect code to see error highlighting.
* Try out all the mappings in ``init.lua`` to learn what they do.

You will still need to regenerate ``compile_commands.json`` every now and then
as you add more sources and change the options/contants used in your build.

Conclusion
----------

You should now have a good baseline for C++ development.  There are many more
non-C++-specific plugins that can be used to improve your dev environment
further, but installing many plugins at once can lead to bloat and too many
things to learn at once.

I suggest to wait a bit, look into ``init.lua`` and learn/tweak the provided
mappings. Take note of the ``<leader>`` mapping, set to ``<Space>``, and the
Telescope mappings.  Once you're used to it, it's better to improve your config
one plugin at a time, taking the time to evaluate and learn to work with each
plugin.


Homework
--------

* In the  ``telescope.nvim`` `README <https://github.com/nvim-telescope/telescope.nvim>`_ 
  you can find many more useful features you may want to map, e.g.
  ``git_files``, ``lsp_references`` or ``man_pages``.

* Find more plugins on `awesome-neovim
  <https://github.com/rockerBOO/awesome-neovim>`_ and `neovimcraft
  <https://neovimcraft.com>`_

* ``nvim-cmp`` has extensions such as `cmp-buffer
  <https://github.com/hrsh7th/cmp-buffer>`_ or `cmp-calc
  <https://github.com/hrsh7th/cmp-calc>`_.



Bonus: performance
------------------

After some testing, neovim was not as fast as I wanted it to be so I made some changes:

- ``use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make'}``

  And in ``require('telescope').setup {``::

     extensions = {
       fzf = {
         fuzzy = true,
         override_generic_sorter = true,
         override_file_sorter = true,
         case_mode = "smart_case",
       }
     }

  This replaces ``telescope``'s default Lua fuzzy matching with a C
  implementation of ``fzf``. This drastically improves ``telescope``
  performance, from 'slow' to 'instant'.

  This requires ``make`` as well as a C toolchain for installation.
  On Debian/Ubuntu derivatives, you can get this with ``sudo apt install build-essential``.

  Don't forget ``:PackerUpdate`` to install ``telescope-fzf-native.nvim``.
- ``completion = { keyword_length = 2 },``

  This is in ``cmp.setup {`` code; it only shows completion suggestions after
  typing 3 characters.  This somewhat decreases CPU overhead of ``nvim-cmp``,
  and it was annoying to be given suggestions right after typing the first
  character.
- I simplified the ``['<Tab>']``/``['<S-Tab>`]`` mappings of ``nvim-cmp``.
  Using ``<Tab>`` to cycle the autocompletion menu lagged compared to using
  ``<Up>`` and ``<Down>``. I rewrote these mappings to match default ``nvim-cmp``
  mappings for ``<C-n>``/``<C-p>``::

     ['<Tab>'] = cmp.mapping(cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }), { 'i', 'c' }),
     ['<S-Tab>'] = cmp.mapping(cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }), { 'i', 'c' }),

  This makes the ``luasnip`` snippets unusable... but I couldn't get
  those to work well with C++ (or at least I couldn't find a non-useless set of
  snippets, as there is no official set).

  I'm currently running without snippets, and will probably try again with
  something like ``vsnip``, or ``UltiSnips``, which in my experience is far
  superior to the vscode snippets ``luasnip`` uses.



Full config repo
----------------

A finished neovim configuration based on this tutorial can be found `on my codeberg <https://codeberg.org/Kiith/kickstart-cpp.nvim>`_.


.. _TLDR:

TLDR - quick setup
------------------

This assumes you are on a Debian/Ubuntu-derived distro, you will need to adapt the commands on other distros.

#. ``sudo apt install cargo ccls bear build-essential``
#. ``cargo install ripgrep``
#. ``curl -LO https://github.com/neovim/neovim/releases/latest/download/nvim.appimage``
#. ``chmod u+x nvim.appimage``
#. Move ``nvim.appimage`` to file ``nvim`` in a directory on ``$PATH``, e.g. for ``/usr/local/bin`` you'd do ``sudo mv nvim.appimage /usr/local/bin/nvim``

   #. check with ``which nvim`` if it matches the path you copied to, and correct if needed (e.g. you may need to ``sudo apt remove neovim`` to remove system ``nvim``)

#. ``mv ~/.config/nvim ~/.config/nvim-bkp``
#. ``mv ~/.local/share/nvim ~/.local/share/nvim-bkp``
#. ``cd ~/.config``
#. ``git clone https://codeberg.org/Kiith/kickstart-cpp.nvim.git``
#. ``mv kickstart-cpp.nvim nvim``
#. ``cd $YOUR_PROJECT_ROOT_DIRECTORY``
#. ``bear $YOUR_BUILD_COMMAND`` # or on newer ``bear``, ``bear -- $YOUR_BUILD_COMMAND``
#. ``nvim``

   #. Ignore errors on startup
   #. ``:PacketInstall`` (ignore installation error of ``nvim-treesitter-textobjects``)
   #. ``:q``

#. Optional, if you need to edit the root directory (e.g. because you have git submodules):

   #. ``nvim ~/.local/share/nvim/site/pack/packer/start/nvim-lspconfig/lua/lspconfig/server_configurations/ccls.lua``

      remove ``'.git'`` from ``root_dir``

   #. ``:q``

#. ``nvim $YOUR_CPP_FILE``

   #. ``:LspStart``
   #. Everything should work now, but check ``:LspInfo`` to confirm the root directory and that at least 1 client is attached.
